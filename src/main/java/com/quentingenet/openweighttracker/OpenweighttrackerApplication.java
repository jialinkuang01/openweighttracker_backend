package com.quentingenet.openweighttracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class OpenweighttrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenweighttrackerApplication.class, args);
	}

}
