package com.quentingenet.openweighttracker.service;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quentingenet.openweighttracker.dto.weights.WeightRecordDto;
import com.quentingenet.openweighttracker.dto.weights.WeightsDto;
import com.quentingenet.openweighttracker.entity.AppUserEntity;
import com.quentingenet.openweighttracker.entity.PersonEntity;
import com.quentingenet.openweighttracker.entity.WeightRecordEntity;
import com.quentingenet.openweighttracker.repository.AppUserRepository;
import com.quentingenet.openweighttracker.repository.PersonRepository;
import com.quentingenet.openweighttracker.repository.WeightRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class WeightRecordServiceImpl implements WeightRecordService {

	@Autowired
	WeightRecordRepository weightRecordRepository;

	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	PersonRepository personRepository;

	private final Logger logger = LoggerFactory.getLogger(WeightRecordServiceImpl.class);

	public WeightRecordDto deleteWeightById(Long weightRecordToDeleteId) {

		WeightRecordEntity weightFinded = weightRecordRepository.findById(weightRecordToDeleteId)
				.orElseThrow(() -> new NoSuchElementException());
		WeightRecordDto weightRecordToDelete = new WeightRecordDto();
		weightRecordToDelete.setWeightKgRecord(null);
		weightRecordToDelete.setWeightRecordDate(null);
		weightRecordToDelete.setIdWeightRecord(null);
		logger.info("THIS IS THE WEIGHT ID TO DELETE : {}", weightRecordToDeleteId);
		weightRecordRepository.deleteById(weightRecordToDeleteId);
		logger.info("Weight with id n°{} is deleted now", weightRecordToDeleteId);

		return weightRecordToDelete;
	}

	@Override
	public WeightsDto getAllWeights(Long idUserConnected) {
		logger.info("Seeking for all weights");
		WeightsDto weightsToShow = new WeightsDto();
		PersonEntity personToShow = personRepository.findById(idUserConnected).orElseThrow();
		weightsToShow.setWeightsList(personToShow.getWeightsList());

		return weightsToShow;
	}

	@Override
	public WeightRecordDto getWeight(Long id) {
		logger.info("Seeking for weight with id n°{}", id);
		WeightRecordEntity weightFinded = weightRecordRepository.findById(id).orElseThrow(NoSuchElementException::new);
		WeightRecordDto weightToShow = new WeightRecordDto();
		weightToShow.setIdWeightRecord(weightFinded.getIdWeightRecord());
		weightToShow.setWeightKgRecord(weightFinded.getWeightKgRecord());
		weightToShow.setWeightRecordDate(weightFinded.getWeightRecordDate());
		weightToShow.setPercentBodyWater(weightFinded.getPercentBodyWater());
		weightToShow.setPercentBoneMass(weightFinded.getPercentBoneMass());
		weightToShow.setPercentFatMass(weightFinded.getPercentFatMass());
		weightToShow.setPercentMuscularMass(weightFinded.getPercentMuscularMass());
		return weightToShow;
	}

	@Override
	public WeightRecordDto saveWeightRecordByIdUser(Long idUser, WeightRecordDto weightRecordFromUser) {

		PersonEntity personToSave = personRepository.findById(idUser).orElseThrow();
		WeightRecordEntity newSavedWeightRecord = new WeightRecordEntity();

		newSavedWeightRecord.setPerson(personToSave);
		newSavedWeightRecord.setWeightKgRecord(weightRecordFromUser.getWeightKgRecord());
		newSavedWeightRecord.setPercentBodyWater(weightRecordFromUser.getPercentBodyWater());
		newSavedWeightRecord.setPercentBoneMass(weightRecordFromUser.getPercentBoneMass());
		newSavedWeightRecord.setPercentFatMass(weightRecordFromUser.getPercentFatMass());
		newSavedWeightRecord.setPercentMuscularMass(weightRecordFromUser.getPercentMuscularMass());
		newSavedWeightRecord.setWeightRecordDate(LocalDate.now());

		weightRecordRepository.save(newSavedWeightRecord);

		List<WeightRecordEntity> weightsListToUpdateList = personToSave.getWeightsList();
		weightsListToUpdateList.add(newSavedWeightRecord);
		personRepository.save(personToSave);

		logger.info("New weight :{} kg is saved in database now.", newSavedWeightRecord.getWeightKgRecord());
		return weightRecordFromUser;
	}

	@Override
	public WeightRecordDto updateWeight(Long idWeight, WeightRecordDto weightRecordFromUser) {
		WeightRecordEntity weightRecordToUpdate = weightRecordRepository.findById(idWeight)
				.orElseThrow(NoSuchElementException::new);
		weightRecordToUpdate.setWeightRecordDate(weightRecordToUpdate.getWeightRecordDate());
		weightRecordToUpdate.setWeightKgRecord(weightRecordFromUser.getWeightKgRecord());
		weightRecordToUpdate.setPercentBodyWater(weightRecordFromUser.getPercentBodyWater());
		weightRecordToUpdate.setPercentBoneMass(weightRecordFromUser.getPercentBoneMass());
		weightRecordToUpdate.setPercentFatMass(weightRecordFromUser.getPercentFatMass());
		weightRecordToUpdate.setPercentMuscularMass(weightRecordFromUser.getPercentMuscularMass());

		weightRecordRepository.save(weightRecordToUpdate);

		return weightRecordFromUser;
	}

}
