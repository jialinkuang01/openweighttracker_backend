package com.quentingenet.openweighttracker.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateAppUserDto;
import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateInitialDataDto;
import com.quentingenet.openweighttracker.entity.AppUserEntity;
import com.quentingenet.openweighttracker.entity.InitialDataEntity;
import com.quentingenet.openweighttracker.entity.PersonEntity;
import com.quentingenet.openweighttracker.repository.AppUserRepository;
import com.quentingenet.openweighttracker.repository.InitialDataRepository;
import com.quentingenet.openweighttracker.repository.PersonRepository;
import com.quentingenet.openweighttracker.repository.WeightRecordRepository;

@Service
public class AppUserServiceImpl implements AppUserService {

	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	InitialDataRepository initialDataRepository;

	@Autowired
	WeightRecordRepository weightRecordRepository;

	@Autowired
	PersonRepository personRepository;

	@Override
	@Transactional
	public AppUserEntity saveUser(AppUserEntity appUser) {
		AppUserEntity appUserToCreate = new AppUserEntity();
		appUserToCreate.setAppUsername(appUser.getAppUsername().toLowerCase());
		appUserToCreate.setPassword(new BCryptPasswordEncoder().encode(appUser.getPassword()));
		appUserRepository.save(appUserToCreate);

		InitialDataEntity initialDataToCreate = new InitialDataEntity();
		initialDataToCreate.setIdInitialData(null);
		initialDataToCreate.setBodySize(null);
		initialDataToCreate.setGoalWeight(null);
		initialDataToCreate.setSex(null);
		initialDataToCreate.setYearBirth(null);

		PersonEntity personUserPerson = new PersonEntity();
		personUserPerson.setUserInitData(initialDataToCreate);
		personUserPerson.setWeightsList(null);

		AppUserEntity appUserCreated = appUserRepository.findByAppUsername(appUser.getAppUsername());
		personUserPerson.setAppUserPerson(appUserCreated);

		personRepository.save(personUserPerson);

		return appUserCreated;
	}

	@Override
	public AppUserEntity findUserByAppUsername(String appUsername) {
		return appUserRepository.findByAppUsername(appUsername);

	}

	@Override
	public AppUserEntity findAppUserById(Long idUser) {
		return appUserRepository.findById(idUser).orElseThrow();
	}

	@Override
	public PersonUpdateInitialDataDto updateInitialDataPerson(Long appUserId,
			PersonUpdateInitialDataDto personToUpdateFromUser) {
		PersonEntity personUpdated = personRepository.findById(appUserId).orElseThrow();

		personUpdated.getUserInitData().setBodySize(personToUpdateFromUser.getBodySize());
		personUpdated.getUserInitData().setGoalWeight(personToUpdateFromUser.getGoalWeight());
		personUpdated.getUserInitData().setSex(personToUpdateFromUser.getSex());
		personUpdated.getUserInitData().setYearBirth(personToUpdateFromUser.getYearBirth());

		personRepository.save(personUpdated);

		return personToUpdateFromUser;
	}

	@Override
	public PersonUpdateAppUserDto updateAppUserPerson(Long appUserId, PersonUpdateAppUserDto personToUpdateFromUser) {

		PersonEntity personUpdated = personRepository.findById(appUserId).orElseThrow();

		personUpdated.getAppUserPerson().setAppUsername(personToUpdateFromUser.getAppUsername());
		personUpdated.getAppUserPerson()
				.setPassword(new BCryptPasswordEncoder().encode(personToUpdateFromUser.getPassword()));

		personRepository.save(personUpdated);

		return personToUpdateFromUser;
	}

}
