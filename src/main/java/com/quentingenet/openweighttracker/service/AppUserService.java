package com.quentingenet.openweighttracker.service;

import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateAppUserDto;
import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateInitialDataDto;
import com.quentingenet.openweighttracker.entity.AppUserEntity;

public interface AppUserService {

	AppUserEntity saveUser(AppUserEntity appUser);
	
	AppUserEntity findUserByAppUsername (String appUsername);
	
	PersonUpdateInitialDataDto updateInitialDataPerson(Long appUserId, PersonUpdateInitialDataDto personToUpdateFromUser);
	
	PersonUpdateAppUserDto updateAppUserPerson(Long appUserId, PersonUpdateAppUserDto personToUpdateFromUser);

	AppUserEntity findAppUserById(Long idUser);

}
