package com.quentingenet.openweighttracker.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "weight_record")
public class WeightRecordEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_weight_record", nullable = true)
	private Long idWeightRecord;
	
	@Column(name = "weight_record_date", nullable = true)
	private LocalDate weightRecordDate;

	//JsonIgnore annotation because circular problem with Jackson and entity.
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private PersonEntity person;
	
	@Min(2)
	@Max(635)
	@Column(name = "weight_kg_record", nullable = true, precision = 1)
	private Double weightKgRecord;

	@Min(2)
	@Max(99)
	@Column(name = "percent_fat_mass", nullable = true, precision = 1)
	private Double percentFatMass;

	@Min(2)
	@Max(99)
	@Column(name = "percent_muscular_mass", nullable = true, precision = 1)
	private Double percentMuscularMass;

	@Min(2)
	@Max(99)
	@Column(name = "percent_body_water", nullable = true, precision = 1)
	private Double percentBodyWater;

	@Min(2)
	@Max(99)
	@Column(name = "percent_bone_mass", nullable = true, precision = 1)
	private Double percentBoneMass;

	/* Getters and setters */
	public Long getIdWeightRecord() {
		return idWeightRecord;
	}

	public void setIdWeightRecord(Long idWeightRecord) {
		this.idWeightRecord = idWeightRecord;
	}

	public LocalDate getWeightRecordDate() {
		return weightRecordDate;
	}

	public void setWeightRecordDate(LocalDate weightRecordDate) {
		this.weightRecordDate = weightRecordDate;
	}

	public Double getWeightKgRecord() {
		return weightKgRecord;
	}

	public void setWeightKgRecord(Double weightKgRecord) {
		this.weightKgRecord = weightKgRecord;
	}

	public Double getPercentFatMass() {
		return percentFatMass;
	}

	public void setPercentFatMass(Double percentFatMass) {
		this.percentFatMass = percentFatMass;
	}

	public Double getPercentMuscularMass() {
		return percentMuscularMass;
	}

	public void setPercentMuscularMass(Double percentMuscularMass) {
		this.percentMuscularMass = percentMuscularMass;
	}

	public Double getPercentBodyWater() {
		return percentBodyWater;
	}

	public void setPercentBodyWater(Double percentBodyWater) {
		this.percentBodyWater = percentBodyWater;
	}

	public Double getPercentBoneMass() {
		return percentBoneMass;
	}

	public void setPercentBoneMass(Double percentBoneMass) {
		this.percentBoneMass = percentBoneMass;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}
	

}
