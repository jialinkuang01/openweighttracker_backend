
package com.quentingenet.openweighttracker.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Table(name = "initial_data")
@Entity
public class InitialDataEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_initial_data")
	private Long idInitialData;

	@Min(5)
	@Max(636)
	@Column(name = "goal_weight", nullable = true, precision = 1)
	private Double goalWeight;

	@Min(25)
	@Max(300)
	@Column(name = "body_size", nullable = true, length = 5)
	private Integer bodySize;

	//Enumeration for gender ?
	//@Size(min = 3, max = 5, message = "Please choose between MAN or WOMAN")
	@Column(name = "sex", nullable = true, length = 10)
	private String sex;

	@Min(1900)
	@Max(2022)
	@Column(name = "year_birth", nullable = true , length = 5)
	private Integer yearBirth;

	/* Getters and setters */
	public Long getIdInitialData() {
		return idInitialData;
	}

	public void setIdInitialData(Long idInitialData) {
		this.idInitialData = idInitialData;
	}

	public Double getGoalWeight() {
		return goalWeight;
	}

	public void setGoalWeight(Double goalWeight) {
		this.goalWeight = goalWeight;
	}

	public Integer getBodySize() {
		return bodySize;
	}

	public void setBodySize(Integer bodySize) {
		this.bodySize = bodySize;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getYearBirth() {
		return yearBirth;
	}

	public void setYearBirth(Integer yearBirth) {
		this.yearBirth = yearBirth;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
