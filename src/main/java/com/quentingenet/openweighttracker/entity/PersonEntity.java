package com.quentingenet.openweighttracker.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class PersonEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_person")
	private Long idPerson;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_initial_data")
	private InitialDataEntity userInitData;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user")
	private AppUserEntity appUserPerson;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,  orphanRemoval = true, mappedBy = "person")
	private List<WeightRecordEntity> weightsList = new ArrayList<WeightRecordEntity>();
	
	/* Getters and setters */
	public Long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(Long idPerson) {
		this.idPerson = idPerson;
	}

	public InitialDataEntity getUserInitData() {
		return userInitData;
	}

	public void setUserInitData(InitialDataEntity userInitData) {
		this.userInitData = userInitData;
	}

	public AppUserEntity getAppUserPerson() {
		return appUserPerson;
	}

	public void setAppUserPerson(AppUserEntity appUserPerson) {
		this.appUserPerson = appUserPerson;
	}

	public List<WeightRecordEntity> getWeightsList() {
		return weightsList;
	}

	public void setWeightsList(List<WeightRecordEntity> weightsList) {
		this.weightsList = weightsList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
