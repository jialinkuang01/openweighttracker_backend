package com.quentingenet.openweighttracker.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.core.userdetails.User;

import com.quentingenet.openweighttracker.security.configuration.MyUserDetailService;

import static com.quentingenet.openweighttracker.security.jwt.JwtFilter.AUTHORIZATION_HEADER;

@RestController
public class JwtController {

	@Autowired
	JwtUtils jwtUtils;

    @Autowired
    MyUserDetailService service;
    
	@Autowired
	AuthenticationManagerBuilder authenticationManagerBuilder;

	@PostMapping("/login")
	public ResponseEntity<?> createAuthToken(@RequestBody JwtRequest jwtRequest) {
		Authentication authentication = logUser(jwtRequest.getAppUsername(), jwtRequest.getPassword());
		String jwt = jwtUtils.generateToken(authentication);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(AUTHORIZATION_HEADER, "Bearer " + jwt);
		Object principal = authentication.getPrincipal();
		 return new ResponseEntity<>(new JwtResponse(((User) principal).getUsername()), httpHeaders, HttpStatus.OK);
	}

	public Authentication logUser(String appUsername, String password) {
		Authentication authentication = authenticationManagerBuilder.getObject()
				.authenticate(new UsernamePasswordAuthenticationToken(appUsername, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		return authentication;
	}
}
