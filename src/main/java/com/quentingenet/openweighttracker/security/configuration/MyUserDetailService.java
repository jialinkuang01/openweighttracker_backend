package com.quentingenet.openweighttracker.security.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.quentingenet.openweighttracker.entity.AppUserEntity;
import com.quentingenet.openweighttracker.repository.AppUserRepository;


@Service
public class MyUserDetailService implements UserDetailsService  {

	@Autowired
	AppUserRepository appUserRepository;

	@Override
	public UserDetails loadUserByUsername(String appUsername) throws UsernameNotFoundException {
		AppUserEntity appUser = appUserRepository.findByAppUsername(appUsername);

		final List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return new org.springframework.security.core.userdetails.User(appUser.getAppUsername(), appUser.getPassword(), authorities);
	}
	
	
	
}
