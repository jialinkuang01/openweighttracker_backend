package com.quentingenet.openweighttracker.dto.appUser;

public class PersonUpdateAppUserDto {

	private String appUsername;

	private String password;

	/* Getters and setters */
	public String getAppUsername() {
		return appUsername;
	}

	public void setAppUsername(String appUsername) {
		this.appUsername = appUsername;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
