package com.quentingenet.openweighttracker.dto.appUser;

public class PersonUpdateInitialDataDto {

	private Double goalWeight;

	private Integer bodySize;

	private String sex;

	private Integer yearBirth;

	/* Getters and setters */
	public Double getGoalWeight() {
		return goalWeight;
	}

	public void setGoalWeight(Double goalWeight) {
		this.goalWeight = goalWeight;
	}

	public Integer getBodySize() {
		return bodySize;
	}

	public void setBodySize(Integer bodySize) {
		this.bodySize = bodySize;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getYearBirth() {
		return yearBirth;
	}

	public void setYearBirth(Integer yearBirth) {
		this.yearBirth = yearBirth;
	}

}
