package com.quentingenet.openweighttracker.controller;

import java.security.Principal;
import java.util.NoSuchElementException;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quentingenet.openweighttracker.GetUserConnectedId;
import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateAppUserDto;
import com.quentingenet.openweighttracker.dto.appUser.PersonUpdateInitialDataDto;
import com.quentingenet.openweighttracker.entity.AppUserEntity;
import com.quentingenet.openweighttracker.entity.PersonEntity;
import com.quentingenet.openweighttracker.entity.WeightRecordEntity;
import com.quentingenet.openweighttracker.repository.AppUserRepository;
import com.quentingenet.openweighttracker.repository.PersonRepository;
import com.quentingenet.openweighttracker.security.jwt.JwtController;
import com.quentingenet.openweighttracker.security.jwt.JwtFilter;
import com.quentingenet.openweighttracker.security.jwt.JwtUtils;
import com.quentingenet.openweighttracker.service.AppUserServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
public class AppUserController {

	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	AppUserServiceImpl appUserServiceImpl;

	@Autowired
	JwtController jwtController;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	PersonRepository personRepository;

	@Autowired
	GetUserConnectedId getUserConnectedId;

	private final Logger logger = LoggerFactory.getLogger(AppUserController.class);

	@PostMapping("/add")
	public ResponseEntity<Object> addUser(@Valid @RequestBody AppUserEntity appUser) {

		logger.info("POST /users/add");

		AppUserEntity existingUser = appUserRepository.findByAppUsername(appUser.getAppUsername());
		if (existingUser != null) {
			return new ResponseEntity<Object>("User already existing", HttpStatus.BAD_REQUEST);
		}
		AppUserEntity appUserSaved = appUserServiceImpl.saveUser(appUser);
		logger.info("NEW USER CREATED IN DATABASE");

		Authentication authentication = jwtController.logUser(appUser.getAppUsername(), appUser.getPassword());
		String jwt = jwtUtils.generateToken(authentication);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(JwtFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

		return new ResponseEntity<Object>(appUserSaved, httpHeaders, HttpStatus.CREATED);
	}

	@DeleteMapping("/{idUser}")
	public ResponseEntity<Object> deleteAppUser(@PathVariable("idUser") Long idUser, Principal principal) {
		logger.info("DELETE /users/{}", idUser);
		Long appUserConnectedId = getUserConnectedId.getAppUserConnectedId(principal);

		try {

			if (appUserConnectedId.equals(idUser)) {
				logger.info("Person is going to be deleted");
				personRepository.deleteById(idUser);
				logger.info("Person with id {} , is deleted now", idUser);
				return new ResponseEntity<Object>("User is deleted", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("User not found", HttpStatus.NOT_FOUND);
			}
		} catch (NoSuchElementException nse) {
			return new ResponseEntity<Object>("User not found", HttpStatus.NOT_FOUND);
		}
	}

	@PatchMapping("/update/initialdata/{appUserId}")
	public ResponseEntity<Object> updateInitialData(@PathVariable("appUserId") Long appUserId,
			@RequestBody PersonUpdateInitialDataDto personToUpdateFromUser, Principal principal) {
		logger.info("DELETE /users/update/{}", appUserId);
		Long appUserConnectedId = getUserConnectedId.getAppUserConnectedId(principal);
		PersonEntity personConnected = personRepository.findById(appUserConnectedId).orElseThrow();
		if (Objects.equals(appUserId, personConnected.getIdPerson())) {
			appUserServiceImpl.updateInitialDataPerson(appUserId, personToUpdateFromUser);
		} else {
			logger.info("BAD USER : UPDATE FOR INITIAL DATA NOT POSSIBLE");
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}

	@PatchMapping("/update/appuser/{appUserId}")
	public ResponseEntity<Object> updateAppUser(@PathVariable("appUserId") Long appUserId,
			@RequestBody PersonUpdateAppUserDto personToUpdateFromUser, Principal principal) {
		Long appUserConnectedId = getUserConnectedId.getAppUserConnectedId(principal);
		PersonEntity personConnected = personRepository.findById(appUserConnectedId).orElseThrow();
		if (Objects.equals(appUserId, personConnected.getIdPerson())) {
			appUserServiceImpl.updateAppUserPerson(appUserId, personToUpdateFromUser);
		} else {
			logger.info("BAD USER : UPDATE FOR APPUSER NOT POSSIBLE");
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}

	// TODO Only admin will can send this request.
	@GetMapping("/all")
	public ResponseEntity<Object> getAllAppUsers(Principal principal) {

		return ResponseEntity.ok().build();
	}
}
