# OpenWeightTracker

***OpenWeightTracker (OWT)*** is a free and opensource application under Copyleft.
<br>This is a simple and easy way to have a look on your  weight ! 
It respect your data, and respect your privacy.

## Technologies

 **OWT** is developed, for the backend, in **Java(JDK17)** with the **framework Spring** with Spring Boot, Data/JPA, and Spring Security.
<br>Concerning front end, application is developed with **React**.


## To run this application...
For the moment, if you want to run project, you must import project's file in your favorite IDE and run it with the 'MainApp' class for the backend and run the app React for the frontend.
You should clone the main branch which is the most updated branch with the final version of OWT app.
In a first time, you must use the **"SignIn"** functionality to create your first user in the app. After doing it, you can login to the app with the user which has just been created adn save your weights.


## Application build details : Database structure diagram
![MPD diagram](https://gitlab.com/genetquentin/openweighttracker_backend/-/raw/main/docs/OWT_MCD.png)

## Ideas and possible improvement
There are possible improvements and ideas... Your suggestions are welcome !

## How to contribute ?
Contributions are welcome, and merge request also ! 
<br>Only a requirement, be a lover of free software and accept copyleft philosophy !

## Author
* Quentin GENET

